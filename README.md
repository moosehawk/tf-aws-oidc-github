# tf-aws-oidc-github

Create a web identity and assumable role via Github OIDC. The role can only be assumed by Github Actions on the `main` branch in the `terraform` repository by default, but that can be modified through inputs.

The identity role is named `github` and has no policies assigned to it. Parent modules should create their own permissions policies and attach them to the role.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.1.9 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 4.13 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 4.13 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_openid_connect_provider.github](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_openid_connect_provider) | resource |
| [aws_iam_role.oidc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.assume_role_oidc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_partition.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/partition) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_github_cert_thumbprint"></a> [github\_cert\_thumbprint](#input\_github\_cert\_thumbprint) | You shouldn't need to change this. Context: <https://github.blog/changelog/2022-01-13-github-actions-update-on-oidc-based-deployments-to-aws> | `string` | `"6938fd4d98bab03faadb97b34396831e3780aea1"` | no |
| <a name="input_github_sub_condition_operator"></a> [github\_sub\_condition\_operator](#input\_github\_sub\_condition\_operator) | Subject condition operator. See <https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_elements_condition_operators.html#Conditions_String> for possible options (you probably want StringEquals or StringLike). | `string` | `"StringEquals"` | no |
| <a name="input_github_sub_condition_value"></a> [github\_sub\_condition\_value](#input\_github\_sub\_condition\_value) | Subject condition value. See <https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_create_for-idp_oidc.html#idp_oidc_Create_GitHub> for examples. By default, a combination of this default and a condition operator of StringEquals will only allow the main branch of the terraform repo assume this role. | `string` | `"repo:moosehawk/terraform-repo:ref:refs/heads/main"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_iam_role_arn"></a> [iam\_role\_arn](#output\_iam\_role\_arn) | ARN of IAM role. |
| <a name="output_iam_role_name"></a> [iam\_role\_name](#output\_iam\_role\_name) | Name of IAM role. |
| <a name="output_oidc_arn"></a> [oidc\_arn](#output\_oidc\_arn) | ARN of the OIDC Provider. |
| <a name="output_oidc_url"></a> [oidc\_url](#output\_oidc\_url) | URL of the OIDC Provider. |
