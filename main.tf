locals {
  github_oidc_url = "token.actions.githubusercontent.com"
}

resource "aws_iam_openid_connect_provider" "github" {
  url = "https://${local.github_oidc_url}"

  client_id_list = [
    "sts.amazonaws.com",
  ]

  thumbprint_list = [
    var.github_cert_thumbprint
  ]
}

data "aws_iam_policy_document" "assume_role_oidc" {
  statement {
    sid     = "1"
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = [aws_iam_openid_connect_provider.github.arn]
    }

    condition {
      test     = "StringEquals"
      variable = "${local.github_oidc_url}:aud"
      values   = ["sts.amazonaws.com"]
    }

    condition {
      test     = var.github_sub_condition_operator
      variable = "${local.github_oidc_url}:sub"
      values   = [var.github_sub_condition_value]
    }
  }
}

resource "aws_iam_role" "oidc" {
  name               = "github"
  description        = "Identity role that can be assumed by Github Actions for deployments."
  assume_role_policy = data.aws_iam_policy_document.assume_role_oidc.json
}
