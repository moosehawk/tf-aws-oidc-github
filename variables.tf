variable "github_cert_thumbprint" {
  description = "You shouldn't need to change this. Context: https://github.blog/changelog/2022-01-13-github-actions-update-on-oidc-based-deployments-to-aws"
  type        = string
  default     = "6938fd4d98bab03faadb97b34396831e3780aea1"
}

variable "github_sub_condition_operator" {
  description = "Subject condition operator. See https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_elements_condition_operators.html#Conditions_String for possible options (you probably want StringEquals or StringLike)."
  type        = string
  default     = "StringEquals"
}

variable "github_sub_condition_value" {
  description = "Subject condition value. See https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_create_for-idp_oidc.html#idp_oidc_Create_GitHub for examples. By default, a combination of this default and a condition operator of StringEquals will only allow the main branch of the terraform repo assume this role."
  type        = string
  default     = "repo:moosehawk/terraform-repo:ref:refs/heads/main"
}
