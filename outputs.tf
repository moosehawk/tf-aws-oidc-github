output "oidc_arn" {
  description = "ARN of the OIDC Provider."
  value       = aws_iam_openid_connect_provider.github.arn
}

output "oidc_url" {
  description = "URL of the OIDC Provider."
  value       = aws_iam_openid_connect_provider.github.url
}

output "iam_role_arn" {
  description = "ARN of IAM role."
  value       = aws_iam_role.oidc.arn
}

output "iam_role_name" {
  description = "Name of IAM role."
  value       = aws_iam_role.oidc.name
}
